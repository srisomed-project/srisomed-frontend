import { StrictMode } from 'react';
import ReactDOM from 'react-dom';
import { MemoryRouter } from 'react-router-dom';
import Root from './pages/Root';
import RootContextProvider from './pages/Root/root.store';

ReactDOM.render(
  <StrictMode>
    <MemoryRouter>
      <RootContextProvider>
        <Root />
      </RootContextProvider>
    </MemoryRouter>
  </StrictMode>,
  document.getElementById('root')
);
