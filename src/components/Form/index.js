import Button from '../Button';
import './form.css';

const Form = ({ children, onSubmit, className }) => {
  return (
    <form
      onSubmit={(e) => {
        e.preventDefault();
        if (onSubmit) onSubmit();
      }}
      className={`${className ? className : ''} form`.trim()}
    >
      {children}
      <Button variant="btn-primary">Submit</Button>
    </form>
  );
};

export default Form;
