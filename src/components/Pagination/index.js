import './pagination.css';
import Button from '../Button';

const Pagination = ({
  currentPage,
  totalPages,
  onFirst,
  onPrevious,
  onNext,
  onLast,
}) => {
  return (
    <div className="pagination">
      {onFirst ? (
        <Button variant="btn-secondary" onClick={onFirst}>
          First
        </Button>
      ) : null}
      <Button variant="btn-secondary" onClick={onPrevious}>
        Previous
      </Button>
      <span className="btn btn-secondary">
        {currentPage}/{totalPages}
      </span>
      <Button variant="btn-secondary" onClick={onNext}>
        Next
      </Button>
      {onLast ? (
        <Button variant="btn-secondary" onClick={onLast}>
          Last
        </Button>
      ) : null}
    </div>
  );
};

export default Pagination;
