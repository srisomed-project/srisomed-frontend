import './article.css';

const Article = ({ title, children, hyperlink }) => {
  return (
    <div className="article">
      <h1>
        {hyperlink ? (
          <a href={hyperlink} target="_blank">
            {title}
          </a>
        ) : (
          title
        )}
      </h1>
      <hr />
      {children}
    </div>
  );
};

export default Article;
