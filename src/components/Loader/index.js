import './loader.css';
import Logo from '../../assets/logo.svg';

const Loader = ({ loading }) => {
  return (
    <div className={`${loading ? 'LoaderVisible' : 'LoaderHidden'}`}>
      <img src={Logo} alt="Loading" />
      <span>Loading...</span>
    </div>
  );
};

export default Loader;
