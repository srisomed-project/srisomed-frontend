import Button from '../Button';
import './modal.css';

const Modal = ({ child, shown, closeModal }) => {
  return (
    <div className={`overlay ${shown ? 'visible' : 'hidden'}`}>
      <Button variant="btn-primary" onClick={() => closeModal()}>
        ✕
      </Button>
      {child}
    </div>
  );
};

export default Modal;
