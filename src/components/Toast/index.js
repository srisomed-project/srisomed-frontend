import { useEffect, useState } from 'react';
import './toast.css';

const Toast = ({ message, shown, variant }) => {
  return (
    <div className={shown ? 'toast toastShown ' + variant : 'toast ' + variant}>
      {message}
    </div>
  );
};

export default Toast;
