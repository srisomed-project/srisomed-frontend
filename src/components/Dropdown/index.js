import './dropdown.css';
import { useEffect, useRef, useState } from 'react';

const Dropdown = ({ options, selected, label, onSelect }) => {
  const [hidden, setHidden] = useState(true);
  const [selectorHeight, setSelectorHeight] = useState(0);
  const selector = useRef();

  useEffect(() => {
    reposition();
  }, [hidden]);

  useEffect(() => {
    reposition();
  }, [selected]);

  const toggleOn = (event) => {
    setHidden(false);
  };

  const toggleOff = () => {
    setHidden(true);
  };

  const toggle = () => {
    if (hidden) setHidden(false);
    else setHidden(true);
  };

  const reposition = () => {
    setSelectorHeight(selector.current.clientHeight);
  };

  // move first .map outside
  const filteredOptions = options
    .map((option) => ({
      id: option._id,
      name: option[Object.keys(option)[1]] || '',
    }))
    .sort((a, b) => (a.name > b.name ? 1 : -1))
    .map((option, key) => (
      <option
        key={key}
        className={`${
          selected.filter((elm) => elm.name === option.name).length > 0
            ? 'selected'
            : 'not-selected'
        }`}
        onClick={(event) => {
          event.preventDefault();
          onSelect(option);
        }}
      >
        {option.name ? option.name : 'N/A'}
      </option>
    ));

  return (
    <div
      className="dropdown"
      // onFocus={toggleOn}
      onBlur={toggleOff}
      onKeyUp={reposition}
      tabIndex="0"
    >
      <div className="selector" ref={selector} onClick={toggle}>
        <span className="label">{label}</span>
        {selected?.map((selectedElement, key) => (
          <div className="selectedOption" key={key}>
            <span>{selectedElement.name ? selectedElement.name : 'N/A'}</span>
          </div>
        ))}
        <span className={`arrow ${hidden ? '' : 'rotate'}`}>↓</span>
      </div>
      <div
        style={{ top: selectorHeight + 10 }}
        className={`options ${hidden ? `hide ` : ''}`.trim()}
      >
        {filteredOptions.length > 0 ? (
          filteredOptions
        ) : (
          <span>No Results...</span>
        )}
      </div>
    </div>
  );
};

export default Dropdown;
