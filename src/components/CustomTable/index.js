import './customTable.css';
import { useState } from 'react';
import Button from '../Button';

/*
  Input Data should be organized as shown below. 
  Add the main attribute to the object, inside the row, to show it in the main section.
  [
    [
      { key: 'First Name', value: 'A', main: true, hidden: true },
      { key: 'Last Name', value: 'B', main: true },
      { key: 'Age', value: '1', main: true },
      { key: 'Biography', value: 'A' },
      { key: 'Degree', value: 'A' },
      { key: 'No', value: '42' },
    ],
    [
      { key: 'fname', value: 'something', main: true },
      { key: 'mname', value: 'something2' },
      ...
    ]
  ]
  */

export const CustomTable = ({ data, exports, addToExports }) => {
  return (
    <div className="customTable">
      {data.map((row, index) => {
        let maindata = row.filter(({ main, hidden }) => main && !hidden);
        let secondarydata = row.filter(({ main, hidden }) => !main && !hidden);
        return (
          <Row
            key={index}
            maindata={maindata}
            secondarydata={secondarydata}
            addToExports={() => {
              addToExports(index);
            }}
            exports={exports}
            _id={row.filter(({ key }) => key === '_id')[0].value}
          />
        );
      })}
    </div>
  );
};

const Row = ({ maindata, secondarydata, addToExports, exports, _id }) => {
  const [shown, setShown] = useState(true);

  const toggleShown = () => {
    setShown(!shown);
  };
  return (
    <div className="row-table">
      <Main data={maindata} />
      <Secondary data={secondarydata} shown={shown} />
      <div className="btns">
        {secondarydata ? (
          <Button variant="btn-secondary" onClick={toggleShown}>
            {shown ? '↑' : '↓'}
          </Button>
        ) : null}
        {addToExports ? (
          <Button
            variant={
              exports.filter((elm) => elm._id === _id).length > 0
                ? 'btn-primary'
                : 'btn-secondary'
            }
            onClick={addToExports}
          >
            Export
          </Button>
        ) : null}
      </div>
    </div>
  );
};

const Main = ({ data }) => {
  return (
    <div className="main-table">
      {data.map((element, key) => (
        <div key={key}>
          <span>{element.key}</span>
          <span>{element.value}</span>
        </div>
      ))}
    </div>
  );
};

const Secondary = ({ shown, data }) => {
  return (
    <div className={`secondary-table ${shown ? 'shown' : ''}`}>
      {data.map((element, key) => (
        <div key={key}>
          <span>{element.key}</span>
          <span>{element.value}</span>
        </div>
      ))}
    </div>
  );
};
