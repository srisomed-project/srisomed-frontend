import './navbar.css';

const NavBar = ({ children, ...props }) => {
  return <div className="navbar">{children}</div>;
};

export default NavBar;
