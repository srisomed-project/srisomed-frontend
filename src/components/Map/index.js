import './map.css';
import { MapContainer, TileLayer } from 'react-leaflet';
import MarkerClusterGroup from 'react-leaflet-markercluster';

const Map = ({ origin, markers }) => {
  return (
    <>
      <MapContainer
        maxBounds={[
          [-90, -180],
          [90, 180],
        ]}
        center={origin}
        zoom={3}
        minZoom={3}
        maxBoundsViscosity={1.0}
      >
        <TileLayer
          attribution="Tiles &copy; Esri &mdash; Source: Esri, DeLorme, NAVTEQ, USGS, Intermap, iPC, NRCAN, Esri Japan, METI, Esri China (Hong Kong), Esri (Thailand), TomTom, 2012"
          url="https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}"
        />
        <MarkerClusterGroup>{markers}</MarkerClusterGroup>
      </MapContainer>
    </>
  );
};

export default Map;
