import './button.css';
const Button = ({ children, variant, ...props }) => (
  <button className={`btn ${variant}`} {...props}>
    {children}
    {props.tooltip ? (
      <span
        tabIndex="-1"
        className="btn-tooltip group-hover:scale-100 group-hover:opacity-100"
      >
        {props.tooltip}
      </span>
    ) : null}
  </button>
);
export default Button;
