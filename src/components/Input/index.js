import './input.css';

const Input = ({ value, label, type, onChange, onBlur }) => {
  return (
    <input
      className="input focus:ring focus:ring-primary focus:ring-offset-2"
      type={type}
      value={value}
      placeholder={label}
      onChange={onChange}
      onBlur={() => {
        if (onBlur) onBlur();
      }}
    />
  );
};

export default Input;
