export const keys = [
  '87Sr/86Sr',
  'Error',
  'Error type',
  'Country',
  'Site',
  'Accuracy',
  'Latitude',
  'Longitude',
  'Type of sample',
  'Sample code',
  'Lithology',
  'Lithology class 1',
  'Lithology class 2',
  'Lithology class 3',
  'Date of sample',
  'Reference',
];

const validCountries = [
  'Algeria',
  'Albania',
  'Bosnia-Herzegovina',
  'Croatia',
  'Cyprus',
  'Egypt',
  'France',
  'Greece',
  'Italy',
  'Levant',
  'Libya',
  'Monaco',
  'Morocco',
  'Other',
  'Portugal',
  'Slovenia',
  'Spain',
  'Tunisia',
  'Turkey',
];

const validAccuracy = [
  'Accurate',
  'Author Coordinates',
  'No coordinates',
  'Semi-accurate',
];

const validSampleCode = [
  'animal',
  'glass',
  'human',
  'magmatic rock',
  'metamorphic rock',
  'mineral',
  'other',
  'plant',
  'sedimentary rock',
  'soil',
  'water',
];

const validLithology = [
  'ev__',
  'mt__',
  'mtam_',
  'mtampr',
  'mtgr_',
  'mtpu_',
  'mtpusr',
  'mtpycl',
  'NA',
  'pa__',
  'pb_mt',
  'pb_vr',
  'pbammt',
  'pi__',
  'pi_mt',
  'scla_',
  'scmx_',
  'scmxch',
  'scmxev',
  'scmxmt',
  'scmxph',
  'scmxvr',
  'scpu_',
  'scpuch',
  'scpuev',
  'scpumt',
  'scpuph',
  'scpuvr',
  'scpy_',
  'scpymt',
  'scpymt_',
  'scsh_',
  'scshbs',
  'scshch',
  'scshev',
  'scshmt',
  'scss_',
  'scssch',
  'scssev',
  'smmx_',
  'smmxcl',
  'smmxev',
  'smmxmt',
  'smmxsu',
  'smpymt',
  'smshch',
  'smshev',
  'smshmt',
  'smss_',
  'smxgl',
  'ssmx_',
  'ssmxcl',
  'ssmxev',
  'ssmxmt',
  'sspy_',
  'sspymt',
  'ssshmt',
  'ssss_',
  'ssssch',
  'suad_',
  'suds_',
  'sumx_',
  'sumxev',
  'sumxgl',
  'supu_',
  'sush_',
  'suss_',
  'va__',
  'va_mt',
  'vapy_',
  'vb__',
  'vbgrmt',
  'vi__',
  'wb__',
];

const validLithologyClass1 = [
  'acid plutonic rocks',
  'acid volcanic rocks',
  'basic plutonic rocks',
  'basic volcanic rocks',
  'carbonate sedimentary rocks',
  'evaporites',
  'intermediate plutonic rocks',
  'intermediate volcanic rocks',
  'metamorphics',
  'mixed sedimentary rocks',
  'NA',
  'siliciclastic sedimentary rocks',
  'siliciclastic sedimentary rocks',
  'unconsolidated sediments',
  'water bodies',
];

const validLithologyClass2 = [
  '(pure) carbonate',
  'alluvial deposits',
  'coarse grained',
  'dune sands',
  'fine grained',
  'greenstone mentioned',
  'laterites',
  'mafic metamorphic mentioned',
  'mixed grain size',
  'NA',
  'pyroclastic mentioned',
];

const validLithologyClass3 = [
  'black shale mentioned',
  'chert mentioned',
  'fossil plant organic material mentioned',
  'glacial influence mentioned',
  'metamorphic influence mentioned',
  'NA',
  'phosphorous-rich minerals mentioned',
  'subordinate evaporites mentioned',
  'subordinate plutonics mentioned',
  'subordinate sedimentary rocks mentioned',
  'subordinate unconsolidated sediments mentioned',
  'subordinate volcanics mentioned',
];

export const validateHeader = (fileKeys) => {
  return keys
    .map((key) => fileKeys.includes(key))
    .reduce((prev, curr) => prev && curr, true);
};
export const validateContent = (fileContents) => {
  let errors = [];
  fileContents.forEach((row, index) => {
    if (!validCountries.includes(row.Country))
      errors.push({
        message: 'Invalid Country',
        entry: index + 1,
        value: row.Country,
      });
    if (!validAccuracy.includes(row.Accuracy))
      errors.push({
        message: 'Invalid Accuracy',
        entry: index + 1,
        value: row.Accuracy,
      });
    if (!validLithology.includes(row.Lithology))
      errors.push({
        message: 'Invalid Lithology',
        entry: index + 1,
        value: row.Lithology,
      });
    if (!validLithologyClass1.includes(row['Lithology class 1']))
      errors.push({
        message: 'Invalid Lithology class 1',
        entry: index + 1,
        value: row['Lithology class 1'],
      });
    if (!validLithologyClass2.includes(row['Lithology class 2']))
      errors.push({
        message: 'Invalid Lithology class 2',
        entry: index + 1,
        value: row['Lithology class 2'],
      });
    if (!validLithologyClass3.includes(row['Lithology class 3']))
      errors.push({
        message: 'Invalid Lithology class 3',
        entry: index + 1,
        value: row['Lithology class 3'],
      });
    if (!validSampleCode.includes(row['Sample code']))
      errors.push({
        message: 'Invalid Sample code',
        entry: index + 1,
        value: row['Sample code'],
      });
    if (isNaN(Number(row['87Sr/86Sr'])))
      errors.push({
        message: 'Invalid 87Sr/86Sr',
        entry: index + 1,
        value: row['87Sr/86Sr'],
      });
    if (isNaN(Number(row.Longitude)))
      errors.push({
        message: 'Invalid Longitude',
        entry: index + 1,
        value: row.Longitude,
      });
    if (isNaN(Number(row.Latitude)))
      errors.push({
        message: 'Invalid Latitude',
        entry: index + 1,
        value: row.Latitude,
      });
  });
  return errors;
};

export const mapDataToCustomFormat = (data) => {
  return data.map((elm) => {
    return [
      {
        key: '⁸⁷Sr/⁸⁶Sr',
        value: elm.ratio ? elm.ratio : 'N/A',
        main: true,
      },
      {
        key: 'Error',
        value: elm.twotheta ? elm.twotheta : 'N/A',
        main: true,
      },
      {
        key: 'Error type',
        value: elm.notes ? elm.notes : 'N/A',
        main: true,
      },
      {
        key: 'Country/Region',
        value: elm.countryArea.countryArea
          ? elm.countryArea.countryArea
          : 'N/A',
        main: true,
      },
      { key: 'Site', value: elm.site, main: true },
      {
        key: 'Latitude',
        value: elm.latitude ? elm.latitude : 'N/A',
        main: true,
      },
      {
        key: 'Longitude',
        value: elm.longitude ? elm.longitude : 'N/A',
        main: true,
      },
      {
        key: 'Accuracy of coordinates',
        value: elm.accuracy,
        main: true,
      },
      {
        key: 'Type of sample',
        value: elm.typeOfSample ? elm.typeOfSample : 'N/A',
        main: true,
      },
      {
        key: 'Sample code',
        value: elm.sampleCode.sampleCode ? elm.sampleCode.sampleCode : 'N/A',
        main: true,
      },
      {
        key: 'Lithology',
        value: elm.geolithology.geolithology
          ? elm.geolithology.geolithology
          : 'N/A',
        main: false,
      },
      {
        key: 'Lithology class 1',
        value: elm.lithologyClass1 ? elm.lithologyClass1 : 'N/A',
        main: false,
      },
      {
        key: 'Lithology class 2',
        value: elm.lithologyClass2 ? elm.lithologyClass2 : 'N/A',
        main: false,
      },
      {
        key: 'Lithology class 3',
        value: elm.lithologyClass3 ? elm.lithologyClass3 : 'N/A',
        main: false,
      },
      {
        key: 'Date of sample',
        value: elm.dateOfSample ? elm.dateOfSample : 'N/A',
        main: false,
      },
      {
        key: 'Reference',
        value: elm.reference ? elm.reference : 'N/A',
        main: false,
      },
      {
        key: '_id',
        value: elm._id,
        hidden: true,
      },
    ];
  });
};
