import { keys } from './validations';

export function download(filename, text) {
  var element = document.createElement('a');
  element.setAttribute(
    'href',
    'data:text/tab-separated-values;charset=utf-8,' + encodeURIComponent(text)
  );
  element.setAttribute('download', filename);

  element.style.display = 'none';
  document.body.appendChild(element);

  element.click();

  document.body.removeChild(element);
}

//ref: https://stackoverflow.com/questions/61328891/how-to-take-a-tsv-file-and-convert-to-json-in-js
export const tsv2arr = (tsv) => {
  const [headers, ...rows] = tsv
    .trim()
    .split('\n')
    .map((r) => r.split('\t'));
  return rows.reduce(
    (a, r) => [
      ...a,
      Object.assign(
        ...r.map((x, i, _, c = x.trim()) => ({
          [headers[i].trim()]: isNaN(c) ? c : Number(c),
        }))
      ),
    ],
    []
  );
};

export const generateTSV = (exports) => {
  if (exports.length === 0) {
    return;
  }
  let tsv = exports.map(function (row) {
    return [
      row.ratio,
      row.twotheta,
      row.notes,
      row.countryArea.countryArea,
      row.site,
      row.accuracy,
      row.latitude,
      row.longitude,
      row.typeOfSample,
      row.sampleCode.sampleCode,
      row.geolithology.geolithology,
      row.lithologyClass1,
      row.lithologyClass2,
      row.lithologyClass3,
      row.dateOfSample,
      row.reference,
    ].join('\t');
  });
  console.log(tsv);
  tsv.unshift(keys.join('\t')); // add header column
  tsv = tsv.join('\r\n');
  download(`data${Math.trunc(Math.random() * 10000)}.tsv`, tsv);
};

export const generatePDF = (exports) => {
  import('./pdfUtil.js')
    .then((pdfMake) => {
      pdfMake = pdfMake.default;

      const content = exports.map((row) => {
        let item = [
          row.ratio,
          row.twotheta,
          row.notes,
          row.countryArea.countryArea,
          row.site,
          row.accuracy,
          row.latitude,
          row.longitude,
          row.typeOfSample,
          row.sampleCode.sampleCode,
          row.geolithology.geolithology,
          row.lithologyClass1,
          row.lithologyClass2,
          row.lithologyClass3,
          row.dateOfSample,
          row.reference,
        ];
        let line = Object.values(item).map((val, idx) => {
          return `${keys[idx]}: ${val ? val : ''}`;
        });

        return [line, ' '];
      });

      const docDefinition = {
        content,
      };
      pdfMake.createPdf(docDefinition).download();
    })
    .catch((exception) => {});
};

export const generateJSON = (exports) => {
  download(
    `data${Math.trunc(Math.random() * 10000)}.json`,
    JSON.stringify(exports)
  );
};
