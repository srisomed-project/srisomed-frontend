import './team.css';
import Article from '../../components/Article';
import EN from '../../assets/team1.jpg';
import PD from '../../assets/team2.jpg';
import MM from '../../assets/team3.jpg';
import MM2 from '../../assets/team4.jpg';
const Team = () => {
  return (
    <div className="team">
      <Article
        title="Efthymia Nikita"
        hyperlink="https://www.cyi.ac.cy/index.php/starc/about-the-center/starc-our-people/author/859-efthymia-nikita.html"
      >
        <div className="card-body">
          <div>
            <img src={EN} alt="Efthymia Nikita Photo" />
          </div>
          <div>
            <p>
              Science and Technology in Archaeology and Culture Research Centre
            </p>
            <p>The Cyprus Institute</p>
          </div>
        </div>
      </Article>
      <Article
        title="Mahmoud Mardini"
        hyperlink="https://www.cyi.ac.cy/index.php/starc/about-the-center/starc-our-people/author/1058-mahmoud-mardini.html"
      >
        <div className="card-body">
          <div>
            <img src={MM} alt="" />
          </div>
          <div>
            <p>
              Science and Technology in Archaeology and Culture Research Centre
            </p>
            <p>The Cyprus Institute</p>
          </div>
        </div>
      </Article>
      <Article
        title="Patrick Degryse"
        hyperlink="https://www.kuleuven.be/wieiswie/en/person/00006002"
      >
        <div className="card-body">
          <div>
            <img src={PD} alt="" />
          </div>
          <div>
            <p>Department of Geology</p>
            <p>KU Leuven</p>
            <p>Faculty of Archaeology</p>
            <a
              href="https://www.universiteitleiden.nl/en/staffmembers/patrick-degryse#tab-1"
              target="_blank"
            >
              Leiden University
            </a>
          </div>
        </div>
      </Article>
      <Article
        title="Mohamad Mardini"
        hyperlink="https://www.linkedin.com/in/mohamad-mardini-a033bb17a/"
      >
        <div className="card-body">
          <div>
            <img src={MM2} alt="" />
          </div>
          <div>
            <p>Software Developer</p>
          </div>
        </div>
      </Article>
    </div>
  );
};

export default Team;
