import './instruction.css';
import Article from '../../components/Article';

const Instruction = () => {
  return (
    <div className="instruction">
      <Article title="User Guide">
        <p>
          SrIsoMed users can search the database either through the data
          filtering option and/or through the interactive map. Data filtering
          offers different options, as filtering can be by country/region,
          sample type, lithology, and range of ⁸⁷Sr/⁸⁶Sr values. The regional
          search adopts modern-day national borders; for simplicity, the term
          Levant has been adopted to jointly denote countries along the
          Levantine coast and Jordan. The sample types have been categorized as:
          animal, human, plant, water, magmatic rock, metamorphic rock,
          sedimentary rock, mineral, glass, soil and other.
        </p>
        <p>
          The lithology follows the coding of lithological classes and
          subclasses adopted by the Global Lithological Map (GLiM v1.0). The
          option to manually select a range of ⁸⁷Sr/⁸⁶Sr values is useful when
          users wish to see which parts of the Mediterranean exhibit specific
          values to test potential points of origin for individuals or raw
          materials. The utility of this filtering option is maximized when
          simultaneously selecting the sample type; for example, to focus only
          on bioavailable samples in cases of palaeomobility research questions.
          In the results output, the users obtain the ⁸⁷Sr/⁸⁶Sr values and
          associated error, exact type of sample analyzed (instead of the
          simplified categories used for filtering purposes), sampling location
          (site name as well as latitude/longitude coordinates), local lithology
          at the sampling location, sample date, and publication from which the
          information was extracted.
        </p>
        <p>
          The SrIsoMed interactive map shows the locations where there are
          available ⁸⁷Sr/⁸⁶Sr values. The creation of the map was based on the
          latitude/longitude coordinates provided in the original publications.
          If these were not available, they were retrieved by the SrIsoMed team
          through georeferencing the maps given in the original publications.
          Even though every effort has been made to retrieve the location of
          every analyzed sample included in the database, in certain cases this
          was not possible. In such cases, the samples are omitted from the map
          but they are still part of the database and can be retrieved using the
          data filtering option. Note that in the map, several locations are
          clustered together; the more sites a cluster includes, the darker its
          color. However, when zooming in a specific location, these clusters
          start breaking down to subclusters until individual points are
          visible. When clicking with the mouse on any individual point, a
          dialog box appears that presents the ⁸⁷Sr/⁸⁶Sr value and associated
          error, type of sample analyzed, sampling location (site name as well
          as latitude/longitude coordinates), local lithology, sample date, and
          publication.
        </p>
        <strong>
          A note regarding georeferenced coordinates: For all coordinates, in
          the data filtering output and in the interactive map, the users can
          see if these were provided by the authors or retrieved by our team. In
          the latter case, their degree of accuracy is also specified, pending
          on how detailed the maps on which georeferencing was based were. All
          follow the World Geodedic System (WGS84).
        </strong>
      </Article>
    </div>
  );
};

export default Instruction;
