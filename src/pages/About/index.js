import './about.css';
import Article from '../../components/Article';
import Logo from '../../assets/logo.jpg';
import EULogo from '../../assets/EU logo.jpg';
import LogoN3 from '../../assets/logo N3.jpg';
import PiMLogo from '../../assets/PiM logo.jpg';
import RIFLogo from '../../assets/RIF logo.png';
import SFLOGO from '../../assets/SF-LOGO.jpg';
import MetaLogo from '../../assets/MetaLogo.jpg';
import AGLeventis from '../../assets/AGLeventis_Logo.jpg';
import David_Prize from '../../assets/david_prize.png';

const About = () => {
  return (
    <div className="about">
      <Article title="SrIsoMed: Open access database of strontium isotopic values (⁸⁷Sr/⁸⁶Sr) across the Mediterranean">
        <img src={Logo} alt="SrIsoMed Logo" />
        <p>
          <strong>SrIsoMed</strong> is a database of published strontium
          isotopic values (⁸⁷Sr/⁸⁶Sr) across the Mediterranean. It follows the
          example of several recent initiatives towards creating open access
          databases for the isotopes of different chemical elements in different
          parts of the world, and aims to promote palaeomobility and raw
          material provenance studies. The SrIsoMed database currently contains
          over 11,400 ⁸⁷Sr/⁸⁶Sr values of organic and inorganic samples and will
          be continuously updated. Users can search the database using the
          interactive map and/or the data filtering option, as explained in the
          User Guide. We would be grateful if you could contact us in order to
          bring to our attention corrections as well as to contribute additional
          data. Development of the SrIsoMed database was supported by the
          European Regional Development Fund and the Republic of Cyprus through
          the Research and Innovation Foundation [grant number:
          EXCELLENCE/1216/0023], as well as the European Union Horizon 2020
          [grant number: 811068]. Additional support from the A.G. Leventis
          Foundation through the A.G. Leventis Chair in Archaeological Sciences
          at The Cyprus Institute is gratefully acknowledged. This work has also
          been supported by the Dan David Prize.
        </p>
        <strong className="text-sm">
          Disclaimer: The data and associated information included in this
          database come from different sources. They are presented as given in
          the original publications but the accuracy of their collection cannot
          be verified.
        </strong>
        <div className="logos">
          <div className="flex justify-center grow">
            <img src={EULogo} alt="EU Logo" />
            <img src={LogoN3} alt="Promise logo" />
            <img src={MetaLogo} alt="Meta Mobility logo" />
          </div>
          <div className="flex justify-center grow">
            <img
              src={AGLeventis}
              alt="AG Leventis Logo"
              className="w-auto h-36"
            />
            <img src={David_Prize} alt="David Prize logo" />
          </div>
          <div className="flex justify-center grow">
            <img src={PiMLogo} alt="People in motion logo" />
            <img
              src={SFLOGO}
              alt="Structural Funds of the European Union in Cyprus"
            />
          </div>
        </div>
        <img src={RIFLogo} alt="Research Innovation Foundation logo" />
      </Article>
    </div>
  );
};

export default About;
