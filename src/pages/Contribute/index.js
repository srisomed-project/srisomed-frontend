import './contribute.css';
import Form from '../../components/Form';
import Files from 'react-files';
import Article from '../../components/Article';
import { useContext, useReducer, useState } from 'react';
import { rootDispatchContext, rootStateContext } from '../Root/root.store';
import Button from '../../components/Button';
import { keys } from '../../utils/validations';
import { tsv2arr, download } from '../../utils/exporters';
import { validateHeader, validateContent } from '../../utils/validations';

const Contribute = () => {
  const rootState = useContext(rootStateContext);
  const rootDispatch = useContext(rootDispatchContext);
  const [info, setInfo] = useState({ filename: '' });
  const [file, setFile] = useState();
  const [errors, setErrors] = useState([]);

  const inputReducer = (state, { type, payload }) => {
    return {
      ...state,
      [type]: payload,
    };
  };

  const validateFileContents = (fileConents) => {
    if (fileConents.length === 0)
      return setErrors([
        {
          message: 'File is Empty',
          entry: 1,
          value: '',
        },
      ]);
    const validHeader = validateHeader(Object.keys(fileConents[0]));
    if (!validHeader)
      return setErrors([
        {
          message: 'Invalid Header',
          entry: 1,
          value: Object.keys(fileConents[0]).join('\t'),
        },
      ]);
    const validContent = validateContent(fileConents);
    setErrors(validContent);
  };

  const [state, dispatch] = useReducer(inputReducer, {
    country: '',
    site: '',
    latitude: '',
    longitude: '',
    typeOfSample: '',
    sampleCode: '',
    geolithology: '',
    lithologyClass1: '',
    lithologyClass2: '',
    lithologyClass3: '',
    dateOfSample: '',
    ratio: '',
    standardDeviation: '',
    notes: '',
    reference: '',
    feedback: '',
  });

  return (
    <div className="contribute">
      <Article title="Contribute Data">
        <Form
          onSubmit={() => {
            if (errors.length > 0) {
              rootDispatch({
                type: 'SHOW_TOAST',
                payload: { message: 'Check Errors', variant: 'error' },
              });
            } else {
              rootDispatch({
                type: 'START_LOADING',
              });
              fetch(`${rootState.API_URL}/post`, {
                method: 'POST',
                body: JSON.stringify({
                  func: 1,
                  query: {
                    submission: {
                      data: JSON.stringify(file),
                      type: 'submission',
                    },
                  },
                }),
              })
                .then(() => {
                  rootDispatch({
                    type: 'SHOW_TOAST',
                    payload: { message: 'Success', variant: 'success' },
                  });
                  rootDispatch({
                    type: 'STOP_LOADING',
                  });
                })
                .catch((error) =>
                  rootDispatch({
                    type: 'SHOW_TOAST',
                    payload: { message: error, variant: 'error' },
                  })
                );
            }
          }}
        >
          <Button
            variant="btn-primary"
            onClick={() => {
              download('contribute-template.tsv', keys.join('\t'));
            }}
          >
            Sample file template
          </Button>
          <span className="text-lg font-bold mt-2">
            Please download the file below to choose from the set of
            predetermined values.
          </span>
          <Button
            variant="btn-primary mb-2"
            onClick={() => {
              window.open(
                'https://drive.google.com/uc?export=download&id=1UDewl8lXxKrq7XtS_A-L73A_tQDQOXp7'
              );
            }}
          >
            Predetermined values for categorical variables
          </Button>
          <Files
            className="files-dropzone"
            onChange={(e) => {
              setInfo({ filename: e[0].name });
              const fileReader = new FileReader();
              fileReader.onload = (event) => {
                const fileContents = tsv2arr(event.target.result);
                validateFileContents(fileContents);
                setFile(fileContents);
                if (errors.length > 0) {
                  rootDispatch({
                    type: 'SHOW_TOAST',
                    payload: { message: 'Check errors', variant: 'error' },
                  });
                }
              };
              fileReader.readAsText(e[0]);
            }}
            onError={(e) => setInfo({ message: e.message })}
            accepts={['.tsv']}
            maxFileSize={5500000}
            minFileSize={0}
            clickable
          >
            Drop files here or click to upload
          </Files>
          <span>
            {info.message
              ? `Error: ${info.message}`
              : `File to upload: ${info.filename}`}
          </span>
        </Form>
        {errors.map((error, index) => (
          <p key={index} className="text-red-500">
            * {error.message} on line {error.entry} with value "{error.value}"
          </p>
        ))}
      </Article>
      <Article title="Feedback">
        <Form
          onSubmit={() => {
            const { feedback } = state;
            rootDispatch({
              type: 'START_LOADING',
            });
            fetch(`${rootState.API_URL}/post`, {
              method: 'POST',
              body: JSON.stringify({
                func: 1,
                query: {
                  submission: {
                    data: feedback,
                    type: 'feedback',
                  },
                },
              }),
            })
              .then(() => {
                rootDispatch({
                  type: 'SHOW_TOAST',
                  payload: { message: 'Success', variant: 'success' },
                });
                rootDispatch({
                  type: 'STOP_LOADING',
                });
              })
              .catch((error) =>
                rootDispatch({
                  type: 'SHOW_TOAST',
                  payload: { message: error, variant: 'error' },
                })
              );
          }}
        >
          <textarea
            label="Feedback"
            value={state.feedback}
            onChange={(e) => {
              dispatch({ type: 'feedback', payload: e.target.value });
            }}
          />
        </Form>
      </Article>
    </div>
  );
};

export default Contribute;
