import './admin-panel.css';
import Login from './Login';
import AdminDashboard from './AdminDashboard';
import { useContext } from 'react';
import { rootStateContext } from '../Root/root.store';

const AdminPanel = () => {
  const state = useContext(rootStateContext);

  return (
    <div className="admin-panel">
      {state.isLoggedIn && state.accessToken ? <AdminDashboard /> : <Login />}
    </div>
  );
};

export default AdminPanel;
