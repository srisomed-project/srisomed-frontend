import './admin-dashboard.css';
import Article from '../../../components/Article';
import { useContext } from 'react';
import { AdminDispatchContext, AdminStateContext } from '../admin.store';
import { useEffect } from 'react';
import { keys } from '../../../utils/validations';
import Pagination from '../../../components/Pagination';
import Button from '../../../components/Button';

const AdminDashboard = () => {
  const state = useContext(AdminStateContext);
  const dispatch = useContext(AdminDispatchContext);

  useEffect(() => {
    dispatch({
      type: 'GET_DATA',
      payload: { func: '10', page: 1 },
    });
  }, []);

  return (
    <div className="panel">
      <Article title="Submissions">
        <div className="buttons">
          <Button
            variant="btn-primary"
            onClick={() => {
              dispatch({ type: 'POST_DATA', payload: { func: '4' } });
              dispatch({
                type: 'GET_DATA',
                payload: { func: '10', page: 1 },
              });
            }}
          >
            Add all
          </Button>
          <Button
            variant="btn-primary"
            onClick={() => {
              dispatch({ type: 'CLEAR_ALL' });
            }}
          >
            Clear all selected
          </Button>
          <Button
            variant="btn-primary"
            onClick={() => {
              dispatch({ type: 'POST_DATA', payload: { func: '5' } });
              dispatch({
                type: 'GET_DATA',
                payload: { func: '10', page: 1 },
              });
            }}
            disabled={
              state.submissionsToAdd.length <= 0 &&
              state.submissionsToRemove.length <= 0
            }
          >
            Submit selected
          </Button>
        </div>
        {state.submissions.map((submission) => (
          <Submission
            submisson={submission}
            key={submission._id}
            _id={submission._id}
            isAdded={state.submissionsToAdd.includes(submission._id)}
            isRemoved={state.submissionsToRemove.includes(submission._id)}
          />
        ))}
        <Pagination
          currentPage={state.currentPage}
          totalPages={Math.ceil(state.submissionsCount / 1000)}
          onPrevious={() => {
            dispatch({
              type: 'CHANGE_PAGE',
              payload: {
                page: state.currentPage - 1,
              },
            });
          }}
          onNext={() => {
            dispatch({
              type: 'CHANGE_PAGE',
              payload: { page: state.currentPage + 1 },
            });
          }}
        />
      </Article>
      <Article title="Feedback">
        {state.feedback.map(({ feedback }, index) => (
          <p key={index} className="feedback">
            {feedback}
          </p>
        ))}
      </Article>
    </div>
  );
};

const Submission = ({ submisson, _id, isAdded, isRemoved }) => {
  const dispatch = useContext(AdminDispatchContext);

  return (
    <div
      className={`submission ${
        isAdded ? ' border-green-700 bg-green-100' : ''
      } ${isRemoved ? ' border-red-700 bg-red-100' : ''}`}
    >
      {keys.map((key, index) => (
        <span key={index} className="entry">
          <label>{key}</label>
          {submisson[key]}
        </span>
      ))}
      <div className="buttons">
        <Button
          variant="btn-danger"
          onClick={() => {
            dispatch({ type: 'REMOVE_SUBMISSION', payload: { _id } });
          }}
        >
          -
        </Button>
        <Button
          variant="btn-primary"
          onClick={() => {
            dispatch({ type: 'ADD_SUBMISSION', payload: { _id } });
          }}
        >
          +
        </Button>
      </div>
    </div>
  );
};

export default AdminDashboard;
