import './login.css';
import Input from '../../../components/Input';
import Lock from '../../../assets/lock.svg';
import Button from '../../../components/Button';
import { useState, useContext } from 'react';
import { rootDispatchContext } from '../../Root/root.store';
import { AdminDispatchContext } from '../admin.store';

const Login = () => {
  const [password, setPassword] = useState('');
  const dispatch = useContext(AdminDispatchContext);
  const login = () => {
    dispatch({ type: 'LOGIN', payload: { password } });
  };
  return (
    <div className="login">
      <img tabIndex="-1" src={Lock} alt="Lock" />
      <Input
        label="Key"
        onChange={(e) => setPassword(e.target.value)}
        value={password}
      />
      <Button variant="btn-secondary" onClick={login}>
        Login
      </Button>
    </div>
  );
};

export default Login;
