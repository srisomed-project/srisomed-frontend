import {
  createContext,
  useContext,
  useEffect,
  useReducer,
  useRef,
} from 'react';
import { rootDispatchContext, rootStateContext } from '../Root/root.store';

export const AdminStateContext = createContext();
export const AdminDispatchContext = createContext();

const AdminContextProvider = ({ children }) => {
  const initailState = {
    feedback: [],
    submissions: [],
    submissionsCount: 0,
    currentPage: 1,
    submissionsToAdd: [],
    submissionsToRemove: [],
  };

  const dataReducer = (state, { type, payload }) => {
    switch (type) {
      case 'DATA_RECEIVED': {
        const { changePage, ...data } = payload;
        if (!changePage) data.currentPage = 1;
        return {
          ...state,
          ...data,
        };
      }
      case 'CHANGE_PAGE': {
        if (
          payload.page > 0 &&
          payload.page <= Math.ceil(state.submissionsCount / 1000)
        )
          return {
            ...state,
            currentPage: payload.page,
          };
        else return state;
      }
      case 'REMOVE_SUBMISSION': {
        if (state.submissionsToAdd.includes(payload._id))
          return {
            ...state,
            submissionsToAdd: state.submissionsToAdd.filter(
              (id) => id !== payload._id
            ),
            submissionsToRemove: [
              ...state.submissionsToRemove.filter((id) => id !== payload._id),
              payload._id,
            ],
          };
        else
          return {
            ...state,
            submissionsToRemove: [
              ...state.submissionsToRemove.filter((id) => id !== payload._id),
              payload._id,
            ],
          };
      }
      case 'ADD_SUBMISSION': {
        if (state.submissionsToRemove.includes(payload._id))
          return {
            ...state,
            submissionsToAdd: [
              ...state.submissionsToAdd.filter((id) => id !== payload._id),
              payload._id,
            ],
            submissionsToRemove: state.submissionsToRemove.filter(
              (id) => id !== payload._id
            ),
          };
        else
          return {
            ...state,
            submissionsToAdd: [
              ...state.submissionsToAdd.filter((id) => id !== payload._id),
              payload._id,
            ],
          };
      }
      case 'CLEAR_ALL': {
        return {
          ...state,
          submissionsToAdd: [],
          submissionsToRemove: [],
        };
      }
      default:
        return state;
    }
  };

  const [state, dispatch] = useReducer(dataReducer, initailState);
  const rootState = useContext(rootStateContext);
  const rootDispatch = useContext(rootDispatchContext);

  function dispatchMiddlware(dispatch) {
    return ({ type, payload }) => {
      const { signal } = controllerRef.current;
      switch (type) {
        case 'LOGIN': {
          rootDispatch({
            type: 'START_LOADING',
          });
          fetch(`${rootState.API_URL}/auth/login`, {
            method: 'POST',
            body: JSON.stringify({
              password: payload.password,
            }),
            signal,
          })
            .then((response) => response.json())
            .then((response) => {
              rootDispatch({
                type: 'SET_ACCESS_TOKEN',
                payload: { ...response },
              });
            })
            .catch(() => {
              rootDispatch({
                type: 'SHOW_TOAST',
                payload: {
                  message: `Login failed`,
                  variant: 'error',
                },
              });
            })
            .finally(() => {
              rootDispatch({
                type: 'STOP_LOADING',
              });
            });
          break;
        }
        case 'GET_DATA': {
          rootDispatch({
            type: 'START_LOADING',
          });
          fetch(
            `${rootState.API_URL}/get?func=${payload.func}&page=${state.currentPage}`,
            {
              headers: {
                Authorization: `Bearer ${rootState.accessToken}`,
              },
            }
          )
            .then((response) => response.json())
            .then((response) => {
              dispatch({ type: 'DATA_RECEIVED', payload: response });
            })
            .catch((error) => {
              rootDispatch({
                type: 'ERROR',
                payload: {
                  error: `Something went wrong! ${error}`,
                },
              });
            })
            .finally(() => {
              rootDispatch({
                type: 'STOP_LOADING',
              });
            });
          break;
        }
        case 'POST_DATA': {
          rootDispatch({
            type: 'START_LOADING',
          });
          fetch(`${rootState.API_URL}/post`, {
            method: 'POST',
            headers: {
              Authorization: `Bearer ${rootState.accessToken}`,
            },
            body: JSON.stringify({
              func: payload.func,
              query: {
                dataToAdd: state.submissionsToAdd,
                dataToRemove: state.submissionsToRemove,
              },
            }),
            signal,
          })
            .then((response) => response.json())
            .catch((error) => {
              rootDispatch({
                type: 'ERROR',
                payload: {
                  error: `Something went wrong! ${error}`,
                },
              });
            })
            .finally(() => {
              rootDispatch({
                type: 'STOP_LOADING',
              });
            });
          break;
        }
        default: {
          dispatch({ type, payload });
          break;
        }
      }
    };
  }

  useEffect(() => {
    dispatchMiddlware(dispatch)({
      type: 'GET_DATA',
      payload: { func: '10', page: state.currentPage },
    });
  }, [state.currentPage]);

  const controllerRef = useRef(new AbortController());
  useEffect(() => {
    return () => {
      controllerRef.current.abort();
    };
  }, []);

  return (
    <AdminStateContext.Provider value={state}>
      <AdminDispatchContext.Provider value={dispatchMiddlware(dispatch)}>
        {children}
      </AdminDispatchContext.Provider>
    </AdminStateContext.Provider>
  );
};

export default AdminContextProvider;
