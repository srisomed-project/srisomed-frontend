import './data.css';
import { useContext, useEffect } from 'react';
import Article from '../../components/Article';
import Button from '../../components/Button';
import { CustomTable } from '../../components/CustomTable';
import Dropdown from '../../components/Dropdown';
import Form from '../../components/Form';
import Input from '../../components/Input';
import Pagination from '../../components/Pagination';
import { dataStateContext, dataDispatchContext } from './data.store';
import {
  generateJSON,
  generatePDF,
  generateTSV,
} from './../../utils/exporters';

import { mapDataToCustomFormat } from '../../utils/validations';

const Data = () => {
  const state = useContext(dataStateContext);
  const dispatch = useContext(dataDispatchContext);

  useEffect(() => {
    dispatch({ type: 'GET_DATA', payload: { func: '0' } });
    dispatch({ type: 'GET_DATA', payload: { func: '7' } });
  }, []);

  const exportSelected = () => {
    switch (state.selectedExportType[0].name) {
      case 'TSV': {
        generateTSV(state.exports);
        break;
      }
      case 'PDF': {
        generatePDF(state.exports);
        break;
      }
      case 'JSON': {
        generateJSON(state.exports);
        break;
      }
      default:
        break;
    }
  };

  return (
    <div className="data">
      <Article title="Filter" className="criteria">
        <Form
          onSubmit={() => {
            dispatch({ type: 'POST_DATA', payload: { func: '2' } });
          }}
        >
          <Dropdown
            label="Sample Code"
            options={state.sampleCodes ? state.sampleCodes : []}
            selected={state.selectedSampleCodes}
            onSelect={(selected) => {
              dispatch({
                type: 'SELECT_MULTIPLE',
                payload: {
                  option: 'selectedSampleCodes',
                  value: selected,
                },
              });
            }}
          />
          <Dropdown
            label="Lithology"
            options={state.shortLithologies ? state.shortLithologies : []}
            selected={state.selectedShortLithologies}
            onSelect={(selected) => {
              dispatch({
                type: 'SELECT_MULTIPLE',
                payload: {
                  option: 'selectedShortLithologies',
                  value: selected,
                },
              });
            }}
          />
          <Dropdown
            label="Country/Region"
            options={state.countries ? state.countries : []}
            selected={state.selectedCountries}
            onSelect={(selected) => {
              dispatch({
                type: 'SELECT_MULTIPLE',
                payload: {
                  option: 'selectedCountries',
                  value: selected,
                },
              });
            }}
          />
          <Dropdown
            label="Export Type"
            options={['TSV', 'PDF', 'JSON'].map((elm, idx) => ({
              _id: idx,
              data: elm,
            }))}
            selected={state.selectedExportType}
            onSelect={(selected) => {
              dispatch({
                type: 'SELECT_SINGLE',
                payload: {
                  option: 'selectedExportType',
                  value: [selected],
                },
              });
            }}
          />
          <Dropdown
            label="Total Results per Page"
            options={[10, 20, 50, 100].map((elm, idx) => ({
              _id: idx,
              data: elm,
            }))}
            selected={state.selectedResultsPerPage}
            onSelect={(selected) => {
              dispatch({
                type: 'SELECT_SINGLE',
                payload: {
                  option: 'selectedResultsPerPage',
                  value: [selected],
                },
              });
            }}
          />
          <div className="flex gap-2 items-center justify-between max-w-full ratios">
            Min ⁸⁷Sr/⁸⁶Sr
            <Input
              type="text"
              label="Min ⁸⁷Sr/⁸⁶Sr"
              onBlur={() => {
                const ratio = Number(state.minRatio);
                if (isNaN(ratio) || ratio < 0 || ratio > state.maxRatio)
                  dispatch({
                    type: 'SELECT_SINGLE',
                    payload: {
                      option: 'minRatio',
                      value: 0,
                    },
                  });
              }}
              onChange={(e) => {
                dispatch({
                  type: 'SELECT_SINGLE',
                  payload: {
                    option: 'minRatio',
                    value: e.target.value,
                  },
                });
              }}
              value={state.minRatio}
            />
            Max ⁸⁷Sr/⁸⁶Sr
            <Input
              type="text"
              label="Max ⁸⁷Sr/⁸⁶Sr"
              onBlur={() => {
                const ratio = Number(state.maxRatio);
                if (isNaN(ratio) || ratio > 1 || ratio < state.minRatio)
                  dispatch({
                    type: 'SELECT_SINGLE',
                    payload: {
                      option: 'maxRatio',
                      value: 1,
                    },
                  });
              }}
              onChange={(e) => {
                dispatch({
                  type: 'SELECT_SINGLE',
                  payload: {
                    option: 'maxRatio',
                    value: e.target.value,
                  },
                });
              }}
              value={state.maxRatio}
            />
          </div>
          {state.exports.length > 0 && (
            <Button
              variant="btn-secondary mb-2"
              onClick={(event) => {
                event.preventDefault();
                exportSelected();
              }}
            >
              Export {state.exports.length} Selected
            </Button>
          )}
          {(state.selectedCountries.length ||
            state.selectedShortLithologies.length ||
            state.selectedSampleCodes.length ||
            state.minRatio != 0 ||
            state.maxRatio != 1) && (
            <Button
              variant="btn-secondary mb-2"
              onClick={(event) => {
                event.preventDefault();
                dispatch({ type: 'GENERATE_EXPORT_FILTERED' });
              }}
            >
              Select Filtered
            </Button>
          )}
          {state.exports.length >= 1 && (
            <Button
              variant="btn-secondary mb-2"
              onClick={(event) => {
                event.preventDefault();
                dispatch({ type: 'CLEAR_EXPORTS' });
              }}
            >
              Clear selected
            </Button>
          )}
          <Button
            variant="btn-secondary mb-2"
            onClick={(event) => {
              event.preventDefault();
              console.log(
                state.selectedExportType,
                typeof state.selectedExportType[0].name
              );
              switch (state.selectedExportType[0].name) {
                case 'TSV':
                  window.open(
                    'https://drive.google.com/uc?export=download&id=13-PGbRHH9rBA6FAnJKdy5gdcmUhUWeuG'
                  );
                  break;
                case 'PDF':
                  window.open(
                    'https://drive.google.com/uc?export=download&id=18z_JZVBC_zWrz6tzl1bfdJpzmRAk-l4Q'
                  );
                  break;
                case 'JSON':
                  window.open(
                    'https://drive.google.com/uc?export=download&id=1kvmgfndZRTiLVGpIBOYq2K1aSsoO61ea'
                  );
                  break;
                default:
                  window.open(
                    'https://drive.google.com/uc?export=download&id=13-PGbRHH9rBA6FAnJKdy5gdcmUhUWeuG'
                  );
              }
            }}
          >
            Export All
          </Button>
        </Form>
      </Article>

      <Article title="Results">
        {state.data?.length > 0 ? (
          <>
            <CustomTable
              data={state.data ? mapDataToCustomFormat(state.data) : []}
              addToExports={(index) => {
                dispatch({
                  type: 'ADD_TO_EXPORTS',
                  payload: state.data[index],
                });
              }}
              exports={state.exports}
            />
            <Pagination
              currentPage={state.currentPage}
              totalPages={state.totalPages}
              resultsPerPage={state.selectedResultsPerPage[0].name}
              onFirst={() => {
                dispatch({ type: 'CHANGE_PAGE', payload: { page: 1 } });
              }}
              onPrevious={() => {
                dispatch({
                  type: 'CHANGE_PAGE',
                  payload: {
                    page: state.currentPage - 1,
                  },
                });
              }}
              onNext={() => {
                dispatch({
                  type: 'CHANGE_PAGE',
                  payload: { page: state.currentPage + 1 },
                });
              }}
              onLast={() => {
                dispatch({
                  type: 'CHANGE_PAGE',
                  payload: {
                    page: state.totalPages,
                  },
                });
              }}
            />
          </>
        ) : (
          <Empty />
        )}
      </Article>
    </div>
  );
};

export default Data;

const Empty = () => {
  return <h2 className="text-3xl">No Results Found. </h2>;
};
