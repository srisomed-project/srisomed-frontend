import {
  createContext,
  useContext,
  useEffect,
  useReducer,
  useRef,
} from 'react';
import { rootDispatchContext, rootStateContext } from '../Root/root.store';

export const dataStateContext = createContext();
export const dataDispatchContext = createContext();

const DataContextProvider = ({ children }) => {
  const initailState = {
    data: [],
    currentPage: 1,
    totalPages: 1,
    countries: [],
    geolithology: [],
    shortLithologies: [],
    sampleCodes: [],
    selectedResultsPerPage: [{ _id: 0, name: 10 }],
    selectedExportType: [{ _id: 0, name: 'TSV' }],
    selectedCountries: [],
    selectedGeolithology: [],
    selectedShortLithologies: [],
    selectedSampleCodes: [],
    exports: [],
    minRatio: 0,
    maxRatio: 1,
  };

  const dataReducer = (state, { type, payload }) => {
    switch (type) {
      case 'DATA_RECEIVED': {
        const { changePage, ...data } = payload;
        if (!changePage) data.currentPage = 1;
        return {
          ...state,
          ...data,
        };
      }
      case 'EXPORTS_RECEIVED': {
        return {
          ...state,
          exports: payload,
        };
      }
      case 'SELECT_MULTIPLE': {
        const isInState =
          state[payload.option].filter((elm) => payload.value.name === elm.name)
            .length > 0;

        return isInState
          ? {
              ...state,
              [payload.option]: state[payload.option].filter(
                (elm) => elm.name !== payload.value.name
              ),
            }
          : {
              ...state,
              [payload.option]: [...state[payload.option], payload.value],
            };
      }
      case 'SELECT_SINGLE': {
        return {
          ...state,
          [payload.option]: payload.value,
        };
      }
      case 'CHANGE_PAGE': {
        if (payload.page > 0 && payload.page <= state.totalPages)
          return {
            ...state,
            currentPage: payload.page,
          };
        else return state;
      }
      case 'ADD_TO_EXPORTS': {
        let filtered = state.exports.filter((elm) => elm._id !== payload._id);
        if (filtered.length !== state.exports.length)
          return {
            ...state,
            exports: [...filtered],
          };
        else
          return {
            ...state,
            exports: [...state.exports, payload],
          };
      }
      case 'CLEAR_EXPORTS': {
        return { ...state, exports: [] };
      }
      default:
        return state;
    }
  };

  const [state, dispatch] = useReducer(dataReducer, initailState);
  const rootState = useContext(rootStateContext);
  const rootDispatch = useContext(rootDispatchContext);

  const dispatchMiddlware =
    (dispatch) =>
    ({ type, payload }) => {
      const { signal } = controllerRef.current;
      switch (type) {
        case 'GET_DATA': {
          rootDispatch({
            type: 'START_LOADING',
          });
          fetch(`${rootState.API_URL}/get?func=${payload.func}`)
            .then((response) => response.json())
            .then((response) => {
              if (payload.func === '7') {
                response['shortLithologies'] = Array.from(
                  new Set(
                    response.geolithology.map((elm) =>
                      elm.geolithology.substr(0, 2)
                    )
                  )
                )
                  .map((elm, idx) => ({ _id: idx, geolithology: elm }))
                  .filter((elm) => elm.geolithology !== 'NA');
              }
              dispatch({ type: 'DATA_RECEIVED', payload: response });
            })
            .catch((error) => {
              rootDispatch({
                type: 'ERROR',
                payload: {
                  error: `Something went wrong! ${error}`,
                },
              });
            })
            .finally(() => {
              rootDispatch({
                type: 'STOP_LOADING',
              });
            });
          break;
        }
        case 'POST_DATA': {
          rootDispatch({
            type: 'START_LOADING',
          });
          fetch(`${rootState.API_URL}/post`, {
            method: 'POST',
            body: JSON.stringify({
              func: payload.func,
              query: {
                selectedResultsPerPage: state.selectedResultsPerPage,
                selectedCountries: state.selectedCountries,
                selectedSites: state.selectedSites,
                selectedGeolithology: state.geolithology.filter((element) => {
                  if (
                    state.selectedShortLithologies
                      .map((elm) => elm.name)
                      .includes(element.geolithology.substr(0, 2))
                  )
                    return true;
                  return false;
                }),
                selectedSampleCodes: state.selectedSampleCodes,
                selectedDatesOfSamples: state.selectedDatesOfSamples,
                page: state.currentPage,
                minRatio: Number(state.minRatio),
                maxRatio: Number(state.maxRatio),
              },
            }),
            signal,
          })
            .then((response) => response.json())
            .then((response) => {
              let totalPages = 0;
              if (response.data.papers.length > 0)
                totalPages = Math.ceil(
                  response.data.metadata[0].total /
                    state.selectedResultsPerPage[0].name
                );
              dispatch({
                type: 'DATA_RECEIVED',
                payload: {
                  data: response.data.papers,
                  totalPages,
                  changePage: payload.changePage,
                },
              });
            })
            .catch((error) => {
              rootDispatch({
                type: 'ERROR',
                payload: {
                  error: `Something went wrong! ${error}`,
                },
              });
            })
            .finally(() => {
              rootDispatch({
                type: 'STOP_LOADING',
              });
            });
          break;
        }
        case 'GENERATE_EXPORT_FILTERED': {
          fetch(`${rootState.API_URL}/post`, {
            method: 'POST',
            body: JSON.stringify({
              func: '2',
              query: {
                selectedResultsPerPage: [{ _id: 0, name: '7000' }],
                selectedCountries: state.selectedCountries,
                selectedSites: state.selectedSites,
                selectedGeolithology: state.geolithology.filter((element) => {
                  if (
                    state.selectedShortLithologies
                      .map((elm) => elm.name)
                      .includes(element.geolithology.substr(0, 2))
                  )
                    return true;
                  return false;
                }),
                selectedSampleCodes: state.selectedSampleCodes,
                selectedDatesOfSamples: state.selectedDatesOfSamples,
                page: 1,
                minRatio: Number(state.minRatio),
                maxRatio: Number(state.maxRatio),
              },
            }),
            signal,
          })
            .then((res) => res.json())
            .then((res) => {
              dispatch({ type: 'EXPORTS_RECEIVED', payload: res.data.papers });
            });
          break;
        }
        default: {
          dispatch({ type, payload });
          break;
        }
      }
    };

  useEffect(() => {
    dispatchMiddlware(dispatch)({
      type: 'POST_DATA',
      payload: { func: '2', changePage: true },
    });
  }, [state.currentPage]);

  const controllerRef = useRef(new AbortController());
  useEffect(() => {
    return () => {
      controllerRef.current.abort();
    };
  }, []);

  return (
    <dataStateContext.Provider value={state}>
      <dataDispatchContext.Provider value={dispatchMiddlware(dispatch)}>
        {children}
      </dataDispatchContext.Provider>
    </dataStateContext.Provider>
  );
};

export default DataContextProvider;
