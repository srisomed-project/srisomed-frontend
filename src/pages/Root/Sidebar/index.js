import { Link } from 'react-router-dom';
import './sidebar.css';
import NavBar from '../../../components/NavBar';
import Logo from '../../../assets/logo_square.jpeg';
import Data from '../../../assets/data.svg';
import Map from '../../../assets/map.svg';
import Home from '../../../assets/home.svg';
import About from '../../../assets/about.svg';
import Team from '../../../assets/team.svg';
import Lock from '../../../assets/lock.svg';
import Contribute from '../../../assets/contribute.svg';
import institute from '../../../assets/cyprus-institute.svg';
import Button from '../../../components/Button';

const Sidebar = () => {
  return (
    <div className="sidebar">
      <img tabIndex="-1" src={Logo} alt="Logo" />
      <NavBar>
        <Link to="/">
          <Button
            tabIndex="-1"
            variant="btn-sidebar btn-secondary group"
            tooltip="Home"
          >
            <img tabIndex="-1" src={Home} alt="Home" />
          </Button>
        </Link>
        <Link to="/data">
          <Button
            tabIndex="-1"
            variant="btn-sidebar btn-secondary group"
            tooltip="Data"
          >
            <img tabIndex="-1" src={Data} alt="Data" />
          </Button>
        </Link>
        <Link to="/map">
          <Button
            tabIndex="-1"
            variant="btn-sidebar btn-secondary group"
            tooltip="Map"
          >
            <img tabIndex="-1" src={Map} alt="Map" />
          </Button>
        </Link>
        {/* <Link to="/stats">
          <Button
            tabIndex="-1"
            variant="btn-sidebar btn-secondary group"
            tooltip="Statistics"
          >
            <img tabIndex="-1" src={Stats} alt="Statistics" />
          </Button>
        </Link> */}
        <Link to="/user-submissions">
          <Button
            tabIndex="-1"
            variant="btn-sidebar btn-secondary group"
            tooltip="Contribute"
          >
            <img tabIndex="-1" src={Contribute} alt="Contribute" />
          </Button>
        </Link>
        <Link to="/team">
          <Button
            tabIndex="-1"
            variant="btn-sidebar btn-secondary group"
            tooltip="Team"
          >
            <img tabIndex="-1" src={Team} alt="Team" />
          </Button>
        </Link>
        <Link to="/instructions">
          <Button
            tabIndex="-1"
            variant="btn-sidebar btn-secondary group"
            tooltip="User Guide"
          >
            <img tabIndex="-1" src={About} alt="User Guide" />
          </Button>
        </Link>
      </NavBar>
      <Link to="/admin-panel">
        <Button
          tabIndex="-1"
          variant="btn-sidebar btn-secondary group"
          tooltip="Admin Panel"
        >
          <img tabIndex="-1" src={Lock} alt="Lock" />
        </Button>
      </Link>
      <a tabIndex="-1" href="https://www.cyi.ac.cy/" target="_blank">
        <Button
          tabIndex="-1"
          variant="btn-sidebar btn-secondary group"
          tooltip="The Cyprus Institute"
        >
          <img tabIndex="-1" src={institute} alt="The Cyprus Institute" />
        </Button>
      </a>
    </div>
  );
};

export default Sidebar;
