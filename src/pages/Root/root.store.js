import { createContext, useEffect, useReducer, useState } from 'react';

export const rootStateContext = createContext();
export const rootDispatchContext = createContext();

const rootReducer = (state, { type, payload }) => {
  switch (type) {
    case 'START_LOADING': {
      return {
        ...state,
        loading: true,
      };
    }
    case 'STOP_LOADING': {
      return {
        ...state,
        loading: false,
      };
    }
    case 'ERROR': {
      return { ...state, loading: false, error: payload.error };
    }
    case 'CLOSE_MODAL': {
      return {
        ...state,
        modal: {
          ...state.modal,
          shown: false,
        },
      };
    }
    case 'SHOW_MODAL': {
      return {
        ...state,
        modal: {
          child: payload.child,
          shown: true,
        },
      };
    }
    case 'SHOW_TOAST': {
      return {
        ...state,
        toast: {
          message: payload.message,
          variant: payload.variant ? payload.variant : '',
        },
      };
    }
    case 'CLEAR_TOAST': {
      return { ...state, toast: {} };
    }
    case 'SET_ACCESS_TOKEN': {
      return {
        ...state,
        accessToken: payload.accessToken,
        isLoggedIn: true,
      };
    }
    default:
      return state;
  }
};

const RootContextProvider = ({ children }) => {
  const initailState = {
    loading: true,
    toast: {},
    API_URL: 'https://srisomed.vercel.app/api',
    // API_URL: 'http://localhost:3001/api',
    modal: {
      child: null,
      shown: false,
    },
    isLoggedIn: false,
    accessToken: '',
  };
  const [state, dispatch] = useReducer(rootReducer, initailState);
  let [timeoutId, setTimeoutId] = useState(-1);
  useEffect(() => {
    clearTimeout(timeoutId);
    setTimeoutId(
      setTimeout(() => {
        dispatch({ type: 'CLEAR_TOAST' });
      }, 2000)
    );
  }, [state.toast.message]);

  return (
    <rootStateContext.Provider value={state}>
      <rootDispatchContext.Provider value={dispatch}>
        {children}
      </rootDispatchContext.Provider>
    </rootStateContext.Provider>
  );
};

export default RootContextProvider;
