import { Route, Routes } from 'react-router-dom';
import './root.css';
import About from '../About';
import Sidebar from './Sidebar';
import Map from '../Map';
import Data from '../Data';
import Contribute from '../Contribute';
import AdminPanel from '../Admin-Panel';
import DataContextProvider from '../Data/data.store';
import { rootStateContext, rootDispatchContext } from './root.store';
import { useContext, useEffect } from 'react';
import Loader from '../../components/Loader';
import AdminContextProvider from '../Admin-Panel/admin.store';
import Modal from '../../components/Modal';
import MapContextProvider from '../Map/map.store';
import Instruction from '../Instruction';
import Team from '../Team';
import Toast from '../../components/Toast';

const Root = () => {
  const state = useContext(rootStateContext);
  const dispatch = useContext(rootDispatchContext);

  useEffect(() => {
    dispatch({ type: 'STOP_LOADING' });
  }, []);

  return (
    <>
      <Sidebar />
      <main className="relative">
        <Routes>
          <>
            <Route exact path="/" element={<About />} />
            <Route
              path="/data"
              element={
                <DataContextProvider>
                  <Data />
                </DataContextProvider>
              }
            />
            <Route
              path="/map"
              element={
                <MapContextProvider>
                  <Map />
                </MapContextProvider>
              }
            />
            <Route path="/user-submissions" element={<Contribute />} />
            <Route
              path="/admin-panel"
              element={
                <AdminContextProvider>
                  <AdminPanel />
                </AdminContextProvider>
              }
            />
            <Route path="/instructions" element={<Instruction />} />
            <Route path="/team" element={<Team />} />
          </>
        </Routes>

        <Loader loading={state.loading} />
        <Modal
          child={state.modal.child}
          shown={state.modal.shown}
          closeModal={() => {
            dispatch({ type: 'CLOSE_MODAL' });
          }}
        />
        <Toast
          message={state.toast.message}
          shown={state.toast?.message}
          variant={state.toast?.variant}
        />
      </main>
    </>
  );
};

export default Root;
