import {
  createContext,
  useReducer,
  useContext,
  useEffect,
  useRef,
} from 'react';
import { rootDispatchContext, rootStateContext } from '../Root/root.store';

export const mapStateContext = createContext();
export const mapDispatchContext = createContext();

const MapContextProvider = ({ children }) => {
  const initailState = {
    markers: [],
    markerDetails: {},
    countries: [],
    geolithology: [],
    shortLithologies: [],
    sampleCodes: [],
    selectedCountries: [],
    selectedGeolithology: [],
    selectedSampleCodes: [],
    selectedLimit: [{ _id: 6, name: 'ALL' }],
    selectedShortLithologies: [],
    minRatio: 0,
    maxRatio: 1,
  };

  const renderModal = (child) => {
    const {
      countryArea,
      site,
      longitude,
      latitude,
      accuracy,
      sampleCode,
      typeOfSample,
      dateOfSample,
      geolithology,
      lithologyClass1,
      lithologyClass2,
      lithologyClass3,
      reference,
      standardDeviation,
      ratio,
      notes,
    } = child;
    return (
      <div className="marker-content">
        <span>
          <label>⁸⁷Sr/⁸⁶Sr:</label> {ratio}
        </span>
        <span>
          <label>Error: </label>
          {standardDeviation}
        </span>
        <span>
          <label>Error type:</label> {notes}
        </span>
        <span>
          <label>Country/Region:</label> {countryArea?.countryArea}
        </span>
        <span>
          <label>Site: </label>
          {site}
        </span>
        <span>
          <label>Longitude: </label>
          {longitude}
        </span>
        <span>
          <label>Latitude: </label>
          {latitude}
        </span>
        <span>
          <label>Accuracy of coordinates:</label> {accuracy}
        </span>
        <span>
          <label>Type of sample:</label> {typeOfSample}
        </span>
        <span>
          <label>Sample code:</label> {sampleCode?.sampleCode}
        </span>
        <span>
          <label>Lithology: </label>
          {geolithology?.geolithology}
        </span>
        <span>
          <label>Lithology class 1:</label> {lithologyClass1}
        </span>
        <span>
          <label>Lithology class 2:</label> {lithologyClass2}
        </span>
        <span>
          <label>Lithology class 3:</label> {lithologyClass3}
        </span>
        <span>
          <label>Date of sample:</label> {dateOfSample}
        </span>
        <span>
          <label>Reference: </label>
          {reference}
        </span>
      </div>
    );
  };

  const mapReducer = (state, { type, payload }) => {
    switch (type) {
      case 'SET_COORDINATES': {
        return {
          ...state,
          markers: payload,
        };
      }
      case 'SET_MARKER_DETAIL': {
        return {
          ...state,
          markerDetails: payload,
        };
      }
      case 'SELECT_MULTIPLE': {
        const isInState =
          state[payload.option].filter((elm) => payload.value.name === elm.name)
            .length > 0;

        return isInState
          ? {
              ...state,
              [payload.option]: state[payload.option].filter(
                (elm) => elm.name !== payload.value.name
              ),
            }
          : {
              ...state,
              [payload.option]: [...state[payload.option], payload.value],
            };
      }
      case 'SELECT_SINGLE': {
        return {
          ...state,
          [payload.option]: payload.value,
        };
      }
      case 'DATA_RECEIVED': {
        const { changePage, ...data } = payload;
        if (!changePage) data.currentPage = 1;
        return {
          ...state,
          ...data,
        };
      }
      default:
        return state;
    }
  };
  const [state, dispatch] = useReducer(mapReducer, initailState);
  const rootState = useContext(rootStateContext);
  const rootDispatch = useContext(rootDispatchContext);

  const mapDispatchMiddleware =
    (dispatch) =>
    ({ type, payload }) => {
      const { signal } = controllerRef.current;
      switch (type) {
        case 'FETCH_COORDINATES': {
          rootDispatch({ type: 'START_LOADING' });
          fetch(`${rootState.API_URL}/get?func=8&limit=${payload}`)
            .then((res) => res.json())
            .then((res) => {
              dispatch({ type: 'SET_COORDINATES', payload: res });
            })
            .finally(() => {
              rootDispatch({ type: 'STOP_LOADING' });
            });
          break;
        }
        case 'GET_MARKER_INFO': {
          rootDispatch({ type: 'START_LOADING' });
          fetch(`${rootState.API_URL}/get?func=9&id=${payload}`)
            .then((res) => res.json())
            .then((res) => {
              dispatch({ type: 'SET_MARKER_DETAIL', payload: res.data[0] });
            })
            .finally(() => {
              rootDispatch({ type: 'STOP_LOADING' });
            });
          break;
        }
        case 'GET_DATA': {
          rootDispatch({
            type: 'START_LOADING',
          });
          fetch(`${rootState.API_URL}/get?func=${payload.func}`)
            .then((response) => response.json())
            .then((response) => {
              if (payload.func === '7') {
                response['shortLithologies'] = Array.from(
                  new Set(
                    response.geolithology.map((elm) =>
                      elm.geolithology.substr(0, 2)
                    )
                  )
                )
                  .map((elm, idx) => ({ _id: idx, geolithology: elm }))
                  .filter((elm) => elm.geolithology !== 'NA');
              }
              dispatch({ type: 'DATA_RECEIVED', payload: response });
            })
            .catch((error) => {
              rootDispatch({
                type: 'ERROR',
                payload: {
                  error: `Something went wrong! ${error}`,
                },
              });
            })
            .finally(() => {
              rootDispatch({
                type: 'STOP_LOADING',
              });
            });
          break;
        }
        case 'POST_DATA': {
          rootDispatch({
            type: 'START_LOADING',
          });
          fetch(`${rootState.API_URL}/post`, {
            method: 'POST',
            body: JSON.stringify({
              func: payload.func,
              query: {
                selectedCountries: state.selectedCountries,
                selectedSites: state.selectedSites,
                selectedGeolithology: state.geolithology.filter((element) => {
                  if (
                    state.selectedShortLithologies
                      .map((elm) => elm.name)
                      .includes(element.geolithology.substr(0, 2))
                  )
                    return true;
                  return false;
                }),
                selectedSampleCodes: state.selectedSampleCodes,
                selectedDatesOfSamples: state.selectedDatesOfSamples,
                minRatio: Number(state.minRatio),
                maxRatio: Number(state.maxRatio),
                limit: state.selectedLimit,
              },
            }),
            signal,
          })
            .then((response) => response.json())
            .then((response) => {
              dispatch({
                type: 'DATA_RECEIVED',
                payload: {
                  markers: response,
                },
              });
            })
            .catch((error) => {
              rootDispatch({
                type: 'ERROR',
                payload: {
                  error: `Something went wrong! ${error}`,
                },
              });
            })
            .finally(() => {
              rootDispatch({
                type: 'STOP_LOADING',
              });
            });
          break;
        }
        default:
          dispatch({ type, payload });
      }
    };

  useEffect(() => {
    if (Object.keys(state.markerDetails).length !== 0)
      rootDispatch({
        type: 'SHOW_MODAL',
        payload: {
          child: renderModal(state.markerDetails),
        },
      });
  }, [state.markerDetails]);

  const controllerRef = useRef(new AbortController());
  useEffect(() => {
    return () => {
      controllerRef.current.abort();
    };
  }, []);

  return (
    <mapStateContext.Provider value={state}>
      <mapDispatchContext.Provider value={mapDispatchMiddleware(dispatch)}>
        {children}
      </mapDispatchContext.Provider>
    </mapStateContext.Provider>
  );
};

export default MapContextProvider;
