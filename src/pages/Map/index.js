import './map.css';
import { useContext, useEffect } from 'react';
import MapComponent from '../../components/Map';
import { mapDispatchContext, mapStateContext } from './map.store';
import { Marker } from 'react-leaflet';
import Logo from '../../assets/map.svg';
import L from 'leaflet';
import Article from '../../components/Article';
import Form from '../../components/Form';
import Dropdown from '../../components/Dropdown';
import Input from '../../components/Input';

const Map = () => {
  const state = useContext(mapStateContext);
  const dispatch = useContext(mapDispatchContext);
  const icons = [
    new L.Icon({
      iconUrl: Logo,
      iconRetinaUrl: Logo,
      iconAnchor: null,
      popupAnchor: null,
      shadowUrl: null,
      shadowSize: null,
      shadowAnchor: null,
      iconSize: [64, 64],
      className: 'leaflet-div-icon-red',
    }),
    new L.Icon({
      iconUrl: Logo,
      iconRetinaUrl: Logo,
      iconAnchor: null,
      popupAnchor: null,
      shadowUrl: null,
      shadowSize: null,
      shadowAnchor: null,
      iconSize: [64, 64],
      className: 'leaflet-div-icon-yellow',
    }),
    new L.Icon({
      iconUrl: Logo,
      iconRetinaUrl: Logo,
      iconAnchor: null,
      popupAnchor: null,
      shadowUrl: null,
      shadowSize: null,
      shadowAnchor: null,
      iconSize: [64, 64],
      className: 'leaflet-div-icon-green',
    }),
    new L.Icon({
      iconUrl: Logo,
      iconRetinaUrl: Logo,
      iconAnchor: null,
      popupAnchor: null,
      shadowUrl: null,
      shadowSize: null,
      shadowAnchor: null,
      iconSize: [64, 64],
      className: 'leaflet-div-icon',
    }),
  ];

  const getIcon = (accuracy) => {
    switch (accuracy.toLowerCase().trim()) {
      case 'author coordinates':
        return icons[0];
      case 'semi-accurate':
        return icons[1];
      case 'accurate':
        return icons[2];
      default:
        return icons[3];
    }
  };

  // should've made a dynamic form filter component
  useEffect(() => {
    dispatch({ type: 'FETCH_COORDINATES' });
    dispatch({ type: 'GET_DATA', payload: { func: '7' } });
  }, []);

  return (
    <div className="leaflet">
      <Article title="Filter">
        <Form
          onSubmit={() => {
            dispatch({ type: 'POST_DATA', payload: { func: '3' } });
          }}
        >
          <Dropdown
            label="Sample Code"
            options={state.sampleCodes ? state.sampleCodes : []}
            selected={state.selectedSampleCodes}
            onSelect={(selected) => {
              dispatch({
                type: 'SELECT_MULTIPLE',
                payload: {
                  option: 'selectedSampleCodes',
                  value: selected,
                },
              });
            }}
          />
          <Dropdown
            label="Lithology"
            options={state.shortLithologies ? state.shortLithologies : []}
            selected={state.selectedShortLithologies}
            onSelect={(selected) => {
              dispatch({
                type: 'SELECT_MULTIPLE',
                payload: {
                  option: 'selectedShortLithologies',
                  value: selected,
                },
              });
            }}
          />
          <Dropdown
            label="Country/Region"
            options={state.countries ? state.countries : []}
            selected={state.selectedCountries}
            onSelect={(selected) => {
              dispatch({
                type: 'SELECT_MULTIPLE',
                payload: {
                  option: 'selectedCountries',
                  value: selected,
                },
              });
            }}
          />
          <Dropdown
            label="Limit"
            options={[100, 250, 500, 1000, 3000, 5000, 'ALL'].map(
              (elm, idx) => ({
                _id: idx,
                data: elm,
              })
            )}
            selected={state.selectedLimit}
            onSelect={(selected) => {
              dispatch({
                type: 'SELECT_SINGLE',
                payload: {
                  option: 'selectedLimit',
                  value: [selected],
                },
              });
            }}
          />
          <div className="flex gap-2 items-center justify-between max-w-full ratios">
            Min ⁸⁷Sr/⁸⁶Sr
            <Input
              type="text"
              label="Min ⁸⁷Sr/⁸⁶Sr"
              onBlur={() => {
                const ratio = Number(state.minRatio);
                if (isNaN(ratio) || ratio < 0 || ratio > state.maxRatio)
                  dispatch({
                    type: 'SELECT_SINGLE',
                    payload: {
                      option: 'minRatio',
                      value: 0,
                    },
                  });
              }}
              onChange={(e) => {
                dispatch({
                  type: 'SELECT_SINGLE',
                  payload: {
                    option: 'minRatio',
                    value: e.target.value,
                  },
                });
              }}
              value={state.minRatio}
            />
            Max ⁸⁷Sr/⁸⁶Sr
            <Input
              type="text"
              label="Max ⁸⁷Sr/⁸⁶Sr"
              onBlur={() => {
                const ratio = Number(state.maxRatio);
                if (isNaN(ratio) || ratio > 1 || ratio < state.minRatio)
                  dispatch({
                    type: 'SELECT_SINGLE',
                    payload: {
                      option: 'maxRatio',
                      value: 1,
                    },
                  });
              }}
              onChange={(e) => {
                dispatch({
                  type: 'SELECT_SINGLE',
                  payload: {
                    option: 'maxRatio',
                    value: e.target.value,
                  },
                });
              }}
              value={state.maxRatio}
            />
          </div>
          {Number(state.selectedLimit[0].name) >= 1000 ||
          isNaN(Number(state.selectedLimit[0].name)) ? (
            <p className="text-red-700">
              * Please note searching the database through the interactive map
              may take a few extra seconds due to the volume of the data.
            </p>
          ) : null}
          <p>*Use the filter above and submit to show markers on the map.</p>
        </Form>
      </Article>
      <MapComponent
        origin={{ lat: 55, lng: 49 }}
        markers={state.markers.map((marker) => (
          <Marker
            icon={getIcon(marker.accuracy)}
            key={marker._id}
            position={{ lat: marker.latitude, lng: marker.longitude }}
            eventHandlers={{
              click: (e) => {
                dispatch({ type: 'GET_MARKER_INFO', payload: marker._id });
              },
            }}
          />
        ))}
      />
    </div>
  );
};

export default Map;
