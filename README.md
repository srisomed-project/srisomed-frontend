# Architecture

SrIsoMed uses the MERN architecture with slight modifications.
The web application frontend is hosted on [Glitch](https://www.glitch.com).
The database uses with MongoDB utilizing the free plan on [mLabs](https://www.mlabs.com).
Backend is built with a variant of NodeJS/Express with serverless on [Vercel](https://www.vercel.com) using the free plan.

## Frontend

The project structure split in 4 sections.

- Components are maily composed of atomic components that make up pages.
- Pages are used to by the router to decide on which page to render.
- Assets are pictures and icons used throughout the application.
- Utils is one component used to lazy load the pdfmake component because its 2 Megabytes to be loaded on page load.

### Dependencies

Here's a list of installed dependencies.

- react-leaflet/leaflet
- Openstreetmap using Leaflet | Tiles © Esri — Source: Esri, DeLorme, NAVTEQ, USGS, Intermap, iPC, NRCAN, Esri Japan, METI, Esri China (Hong Kong), Esri (Thailand), TomTom, 2012
- react-router-dom
- pdfmake
- tailwindcss

## Backend

Vercel imposes limits on the amount of endpoints available. To circumvent this limitation, the backend is built with only the basic CRUD operations supported and functions on the crud operations are sent in the body(or url) of the request. For example, if you want to get all stored documents in the database the request would look like this: api/get?func=0.

The project structure is split into 2 sections:

- The api which contains the basic endpoints.
- The helpers is composed of multiple components:
  - The database driver implements high-level communication with the database.
  - The path resolver dictates what path the endpoint should call.
  - The function resolver composes the parameters and calls the chosen function.

## Database

The database is composed of multiple collections. A certain set of collections(lithology, sample codes, etc...) are used as a relation between the main document entries and the filter properties of the collection. This helps when retrieving filters and filtering main documents, the main documents are requested as aggregates, and the filter properties are projected and unwinded to build a complete document.
