module.exports = {
  mode: 'jit',
  content: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: 'media', // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        primary: '#2563F5',
        'primary-light': '#DBEAFE',
        secondary: '#FFFFFF',
        typography: '#1F2937',
        light: '#F3F4F6',
        dark: '#A1A2A6',
      },
      transitionProperty: {
        height: 'height',
      },
    },
  },
  variants: {},
  plugins: [],
  important: true,
};
